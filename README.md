# Node.js POC

- Description: POC Project in Node.js, express.js, R Code, MySql 

# Sever requirements 

- R, 
- MySql, 
- Node.js // >6.9.4

# Code rules

- https://www.npmjs.com/package/node-style-guide

- http://jshint.com/docs/options/

# Running
- $Import mysql db dumbp (ie /sql-dump/poc.sql) 
- $ npm install
- $ export PORT=5000
- $ export DB_HOSTS=localhost //Mysql connection url
- $ export DB_NAME = poc //Mysql db name
- $ export DB_USER = ''// Mysql username
- $ export DB_SECRET = ''//Mysql user password
- $ node app.js

# Login 

- Admin user: 
    - email: remya@amt.in
    
    - password: 12346435##
    - Admin user can see all users Table access and edit them

- Manager user: 

    - email: fermentum@ac.org
    
    - password: 5345@$#@$#@
    
    - Manager users only see their own accesses 



