
jQuery(document).ready(function() {
	
    /*
        Fullscreen background
    */
    //$.backstretch("img/backgrounds/1.jpg");
    
    /*
        Form validation
    */
    $('.login-form input[type="text"], .login-form input[type="password"], .login-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    $('.login-form').on('submit', function(e) {
    	
    	$(this).find('input[type="text"], input[type="password"], textarea').each(function(){
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	
    });
    
    
});
function updateUser(ers){
$('.message').removeClass('alert alert-success');
var email=$('#email'+ers).text();
if($('#gdpr'+ers).prop("checked") == true){
	var gdpr=true;
}
else{
	var gdpr=false;
}
if($('#popr'+ers).prop("checked") == true){
	var popr=true;
}
else{
	var popr=false;
}
if($('#unir'+ers).prop("checked") == true){
	var unir=true;
}
else{
	var unir=false;
}
if($('#gdpw'+ers).prop("checked") == true){
	var gdpw=true;
}
else{
	var gdpw=false;
}
if($('#popw'+ers).prop("checked") == true){
	var popw=true;
}
else{
	var popw=false;
}
if($('#uniw'+ers).prop("checked") == true){
	var uniw=true;
}
else{
	var uniw=false;
}

var database=[{
      "name": "gdp",
      "read": gdpr,
      "write": gdpw
    }, {
      "name": "population",
      "read": popr,
      "write": popw
    }, {
      "name": "unicef",
      "read": unir,
      "write": uniw
    }];
console.info(database)
$.ajax({
	type: 'POST',
	url: '/user/update',
	data: { email: email,database: database},
	success: function (data) {
		console.log(data);
		$('.message').addClass('alert alert-success');
		$('.message').html(data.message);
	},
	error: function (err, data) {
		console.log(data)
	}
});		  

}

