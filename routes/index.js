var UserController = require('./controllers/user-controller');
module.exports = function (app, db) {
    var sanitizeInput = function (req, res, next) {
            var validator = require('validator'),
                sanitizer = require('sanitizer'),
                requestData = req.body;

            for (var key in requestData) {
                if (requestData.hasOwnProperty(key)) {
                    if ('string' === typeof requestData[key]) {
                        req.body[key] = validator.trim(requestData[key]);
                        req.body[key] = sanitizer.sanitize(requestData[key]);
                    }
                }
            }
            next();
        },
        checkRequiredFields = function (requiredFields) {
            return function check(req, res, next) {
                var error = [];
                if (requiredFields) {
                    requiredFields.forEach(function (key) {
                        if (!req.body[key]) {
                            error.push(key);
                        }
                    });
                }
                if (error.length > 0) {
                    var response = {
                        status: false,
                        message: "REQUIRED_FIELDS_MISSING",
                        data: error,
                        code: 412
                    };
                    return next(response);
                } else {
                    next();
                }
            };
        },
        isUser = function (req, res, next) {
            if (req.session && req.session.email) {
                req.email = req.session.email;
                next();
            } else {
                next({status: false, message: 'AUTH_FAILURE', code: 401});
            }
        };

    var userController = new UserController(app, db);
    app.get('/', userController.Home);
    app.post('/login', [sanitizeInput, checkRequiredFields(['email', 'password'])], userController.Login);
    app.post('/logout', [isUser], userController.Logout);
    app.post('/rstats', userController.Rstats);
    app.get('/user-list', [isUser], userController.UserList);
    app.get('/user-access', [isUser], userController.DatabaseAccess);
    app.post('/user/update', [isUser, sanitizeInput, checkRequiredFields(['email'])], userController.UserUpdate);
    app.get('/login', userController.Home);
	app.get('/stats', [isUser], userController.Stats);
    app.get('/high-charts', [isUser], userController.HighChart);
};
