/**
 * Created by Dibeesh KS on 14-02-2017.
 */
//var fs = require('fs');
var _ = require('underscore');
var jsonfile = require('jsonfile');
var async = require('async');
var BaseModel = require('../models/base');
function UserController(app, db) {
    var FetchData = function (req, res, next) {
        return new Promise(
            function (resolve, reject) {
                // Get content from file
                var baseModel = new BaseModel(app, db);
                var file = 'data/data.json';
                var result;
                var found;
                jsonfile.readFile(file, function (err, obj) {
                    var readJson = obj;
                    var results = [];
                    if (req.session.access_level === 'admin') { // Give full access to Admin only
                        result = readJson.users;
                    } else {
                        found = _.where(readJson.users, {email: req.session.email});
                        result = found;
                    }
                    async.eachSeries(result,
                        function (userdata, callback1) {
                            var result = {};
                            result.email = userdata.email;
                            result.access_level = userdata.access_level;
                            var dbResults = [];
                            async.eachSeries(userdata.database,
                                function (dbdata, callback2) {
                                    var dbResult = {};
                                    dbResult.name = dbdata.name;
                                    if (dbdata.read == 'true' || dbdata.read === true) { // Have permission
                                        dbResult.permission = true;
                                        var query = 'SELECT * from ' + dbdata.name + ' LIMIT 5';
                                        req.query = query;
                                        baseModel.GetData(req, res)
                                            .then(function (results) {
                                                dbResult.result = results;
                                                dbResults.push(dbResult);
                                                callback2();
                                            }, function (error) {
                                                //console.log("error on find devie by macid", error);
                                                callback2();
                                            });

                                    } else { // No permission
                                        dbResult.permission = false;
                                        dbResult.result = null;
                                        dbResults.push(dbResult);
                                        callback2();
                                    }

                                },
                                function (err) {
                                    if (err) {
                                        return err;
                                    }
                                    result.database = dbResults;
                                    results.push(result);
                                    callback1();
                                });

                        },
                        function (err) {
                            if (err) {
                                reject(err);
                            }
                            resolve(results);

                        });
                });
            });


    };
    this.Home = function (req, res) {
        if (req.session.email)
            return res.redirect('/user-list');
        else
            res.render('index', {title: 'Login', status: 200});
    };
    this.Login = function (req, res) {
        var email = req.body.email;
        var password = req.body.password;
        var file = 'data/data.json';
        var access_level;
        jsonfile.readFile(file, function (err, obj) {
            var readJson = obj;
            console.log('readJson', readJson);
            var found = _.where(readJson.users, {email: email, password: password});
            if (found.length > 0) {
                access_level = found[0].access_level;
                req.session.access_level = access_level;
                req.session.email = email;
                console.log('req.session.email', req.session.email);
                //res.render('home',{title: 'Login',"status": 200, "message": "Success", email: email});// Replace with render
                return res.redirect('/user-list');
            } else {
                console.log('user not found');
                res.render('index', {title: 'Login', 'status': 404, 'message': 'Invalid Login'});
            }
        });
    };
    this.Logout = function (req, res, next) {
        req.session.email = '';
        req.session.destroy();
        res.render('index', {status: true});
    };
    //LISTING ALL USERS+
    this.UserList = function (req, res) {
        // Get content from file
        var file = 'data/data.json';
        var result;
        var found;
        jsonfile.readFile(file, function (err, obj) {
            var readJson = obj;
            if (req.session.access_level === 'admin') { // Give full access to Admin only
                result = readJson.users;
            } else {
                found = _.where(readJson.users, {email: req.session.email});
                result = found;
            }
            res.render('home', {title: 'Users', 'status': 200, 'users': result, 'email': req.session.email});
        });
    };
    this.Rstats = function (req, res, next) {
        console.log('hello');
        /* var opencpu = require("opencpu");
         opencpu.rCall("/library/datasets/R/mtcars/json", {}, function (err, data) {
         if (!err) {
         console.log(data[0].mpg + data[1].mpg); // => 42
         res.json(true);
         } else {
         console.log("opencpu call failed.", err);
         res.json(false);
         }
         });*/
        /*  var R = require("data/r-script");
         var out = R("ex-sync.R")
         .data("hello world", 20)
         .callSync();

         console.log('output data', out);*/
        /*   var r = require('rserve-client');
         r.connect('http://192.168.2.203', 8787, function(err, client) {
         if(err) {
         console.log('error', err);
         return next(err);
         }
         console.log('success');
         client.evaluate('a<-2.7+2', function(err, ans) {
         console.log(ans);
         client.end();
         });
         });*/

    };
    this.UserUpdate = function (req, res, next) {
        var email = req.body.email;
        var database = req.body.database;
        var file = 'data/data.json';
        jsonfile.readFile(file, function (err, obj) {
            var readJson = obj;
            var arrayIndex = _.findIndex(readJson.users, {
                email: email
            });
            if (arrayIndex !== -1) {
                console.log('found an entry', JSON.stringify(readJson.users[arrayIndex]));
                var findJson = readJson.users[arrayIndex];
                var writeObj = {
                    'email': email,
                    'password': findJson.password,
                    'access_level': findJson.access_level,
                    'database': database
                };
                readJson.users[arrayIndex] = writeObj;
                jsonfile.writeFile(file, readJson, {spaces: 2}, function (err) {
                    if (err) {
                        console.error(err);
                        return next();
                    }
                    res.json({'status': true, 'message': 'Updated user accesses'});
                })

            } else {
                return next({'status': false, 'message': 'Not found user with emailID given'});
            }

        });
    };
    this.DatabaseAccess = function (req, res) {
        // Get content from file
        FetchData(req, res)
            .then(function (results) {
                res.render('access', {title: 'Tables', 'users': results, 'email': req.session.email});
            }, function (error) {
                res.render('access', {title: 'Tables', 'users': [], 'email': req.session.email});
            });
    };
    this.Stats = function (req, res, next) {
        FetchData(req, res)
            .then(function (results) {
                res.render('stats', {title: 'Tables', 'users': results, 'email': req.session.email});
            }, function (error) {
                res.render('stats', {title: 'Tables', 'users': [], 'email': req.session.email});

            });


    }
    this.HighChart = function (req,res) {

	       res.render('highcharts', {title: 'Tables', 'email': req.session.email});

	}
}
module.exports = UserController;