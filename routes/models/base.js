/**
 * Created by Dibeesh KS on 16-02-2017.
 */
function BaseModel(app, db) {
    this.GetData = function (req, res) {
        return new Promise(
            function (resolve, reject) {
                var query = req.query;
                db.query(query, function (error, results) {
                    if (error) {
                        resolve(error);
                    }
                    resolve(results);
                });
            });
    };
}
module.exports = BaseModel;
