/**
 * Created by Dibeesh KS on 14-02-2017.
 */
var config = {
    local: {
        mode: 'local',
        port: 3001
    },
    staging: {
        mode: 'staging',
        port: 4001
    },
    production: {
        mode: 'production',
        port: 5001
    },
    DB_HOSTS: process.env.DB_HOSTS || '192.168.2.203',
    DB_NAME: process.env.DB_NAME || 'poc',
    DB_USER: process.env.DB_USER || 'root',
    DB_SECRET: process.env.DB_SECRET || 'amt123'

};
module.exports = config;
