 /**
 * Created by Dibeesh KS on 14-02-2017.
 */
var config = require('./config');
function database() {
     var mysql      = require('mysql');
    this.DatabaseConnection = function () {
        var connection = mysql.createConnection({
            host     : config.DB_HOSTS || '192.168.2.203',
            user     : config.DB_USER || 'root',
            password : config.DB_SECRET || 'admin123',
            database : config.DB_NAME || 'poc'
        });
        connection.connect();
        return connection;
    };
}
module.exports = database;