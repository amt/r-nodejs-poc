'use strict';
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var compression = require('compression');
var expressSession = require('express-session');


function logErrors(err, req, res, next) {
    console.log('logError', err);
    console.error(err);
    //http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
    if (err.stack) {
        console.error(err.stack);
    }
    next(err);
}

function getErrorCode(err) {
    return 404;
}

function clientErrorHandler(err, req, res, next) {
    console.log('clientErrorHandler');
    var code = getErrorCode(err);
    res.set('Connection', 'close');
    var d = {
        status: err.status,
        info: err.message || 'Something went really wrong!!!',
        code: err.code || 400
    };
    if (err.data) {
        d.data = err.data;
    }
    //res.send(200, d);
    res.status(200).send(d);
}
var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, sessionID, Content-Length, X-Requested-With');
    if ('OPTIONS' === req.method) {
        //return res.send(200);
        return res.status(200).send();
    }
    if (!app.get('endpoint')) {

        app.set('endpoint', 'http://' + req.get('host'));
    }
    next();
};


var app = express();
app.use(compression());
app.use(methodOverride());
app.use(cookieParser());
app.use(expressSession({
    secret: 'uRQ3VEuYq0YNdeLcaNXFEn1UyMU3G4xe',
    resave: true,
    saveUninitialized: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(allowCrossDomain);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.locals.title = 'Nodejs POC';

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));

//Env

var env = process.env.NODE_ENV || 'development';
if ('development' === env) {
    // dev configure stuff here
    console.log('Production');

}
if ('production' === env) {
    // production configure stuff here
    console.log('dev');

}

var Connection = require('./config/database');
var connection = new Connection();
var db = connection.DatabaseConnection();
require('./routes')(app, db);
app.use(logErrors);
app.use(clientErrorHandler);
app.use(redirectUnmatched); 

app.listen(process.env.PORT || 9091, function () {
    console.log('Node POC app listening on port ' + process.env.PORT || 9091);
});


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    console.log('error 404');
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
//, next
app.use(function (err, req, res) {
    // set locals, only providing error in development
    console.log('error', err.message);
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});
function redirectUnmatched(req, res) {
    res.redirect("/");
}

module.exports = app;
