/**
 * Created by Dibeesh KS on 14-02-2017.
 */
var config = {
    password_salt: '',
    index_name: process.env.DB_INDEX_NAME || 'poc'

};
module.exports = config;