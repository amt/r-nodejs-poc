/**
 * Created by Dibeesh KS on 16-02-2017.
 */
describe("Configuration setup", function() {
    it("should load local configurations", function(next) {
        var config = require('../config/config')();
        expect(config.mode).toBe('local');
        next();
    });
    it("should load staging configurations", function(next) {
        var config = require('../config/config')('staging');
        expect(config.mode).toBe('staging');
        next();
    });
    it("should load production configurations", function(next) {
        var config = require('../config/config')('production');
        expect(config.mode).toBe('production');
        next();
    });
});