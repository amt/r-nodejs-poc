/**
 * Created by Dibeesh KS on 16-02-2017.
 */
var request = require("request");

var base_url = "http://192.168.2.2:3000/";

describe("home", function() {
    describe("GET /", function() {
        it("returns status code 200", function() {
            request.get(base_url, function(error, response, body) {
                expect(response.statusCode).toBe(200);

            });

        });
    });
});